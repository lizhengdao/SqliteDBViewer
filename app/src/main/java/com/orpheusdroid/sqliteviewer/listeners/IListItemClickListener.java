package com.orpheusdroid.sqliteviewer.listeners;

/**
 * Todo: Add class description here
 *
 * @author Vijai Chandra Prasad .R
 */
public interface IListItemClickListener {
    void onClick(int position);
}
